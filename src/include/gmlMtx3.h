#ifndef GML_MTX3_H
#define GML_MTX3_H
#pragma once

#include "gmlMath.h"

namespace gml
{
	class qnVec3;
	class qnQuat;

	class GML_EXPORT qnMtx3
	{
	public:
		qnMtx3() {} // No default initialization
		qnMtx3(float a, float b, float c, float d, float e, float f, float g, float h, float i);
		qnMtx3(const qnMtx3& rhs);
		qnMtx3& operator= (const qnMtx3& rhs);

		// Members work in place
		qnMtx3& Scale(const float f); // Scalar multiply

		qnMtx3& Add(const qnMtx3& rhs);
		qnMtx3& Sub(const qnMtx3& rhs);
		qnMtx3& Mul(const qnMtx3& rhs);
		qnMtx3& Transpose();
		qnMtx3& Invert();

		qnMtx3& Stabelize();

		// non Member
		static qnMtx3 Scale(const qnMtx3& mtx, const float f);

		static qnMtx3 Add(const qnMtx3& lhs, const qnMtx3& rhs);
		static qnMtx3 Sub(const qnMtx3& lhs, const qnMtx3& rhs);
		static qnMtx3 Mul(const qnMtx3& lhs, const qnMtx3& rhs);
		static qnMtx3 Transpose(const qnMtx3& mtx);
		static qnMtx3 Invert(const qnMtx3& mtx);

		static qnMtx3 RotateX(float angle);
		static qnMtx3 RotateY(float angle);
		static qnMtx3 RotateZ(float angle);

		static qnMtx3 RotateEuler(float yaw, float pitch, float roll);
		static qnMtx3 RotateAxisAngle(qnVec3 axis, const float angle);
		static qnMtx3 RotateQuaternion(const qnQuat& quaternion);

		static void GetAxisAngle(const qnMtx3& mtx, qnVec3* pAxis, float* pAngle);
		//static void GetEulerAngles( const qnMtx3& mtx, float* yaw, float* pitch, float* roll );

		static qnMtx3 Stabelize(const qnMtx3& mtx);

		union
		{
			struct {
				float _00, _01, _02,
					_10, _11, _12,
					_20, _21, _22;
			} mtx;
			float a[3][3];
			float f[9];
		} data;

		static const qnMtx3 ZERO;
		static const qnMtx3 ONE;
		static const qnMtx3 FLIP_X;
		static const qnMtx3 FLIP_Y;
		static const qnMtx3 FLIP_Z;
		static const qnMtx3 IDENTITY;
	};

	inline qnMtx3 operator+ (const qnMtx3& lhs, const qnMtx3& rhs) { return qnMtx3::Add(lhs, rhs); }
	inline qnMtx3 operator- (const qnMtx3& lhs, const qnMtx3& rhs) { return qnMtx3::Sub(lhs, rhs); }
	inline qnMtx3 operator* (const float scale, const qnMtx3& rhs) { return qnMtx3::Scale(rhs, scale); }
	inline qnMtx3 operator* (const qnMtx3& lhs, const float scale) { return qnMtx3::Scale(lhs, scale); }
	inline qnMtx3 operator* (const qnMtx3& lhs, const qnMtx3& rhs) { return qnMtx3::Mul(lhs, rhs); }

	inline qnMtx3& operator+= (qnMtx3& lhs, const qnMtx3& rhs) { return lhs.Add(rhs); }
	inline qnMtx3& operator-= (qnMtx3& lhs, const qnMtx3& rhs) { return lhs.Sub(rhs); }
	inline qnMtx3& operator*= (qnMtx3& lhs, const float scale) { return lhs.Scale(scale); }
	inline qnMtx3& operator*= (qnMtx3& lhs, const qnMtx3& rhs) { return lhs.Mul(rhs); }

	bool GML_EXPORT operator== (const qnMtx3& lhs, const qnMtx3& rhs);
	inline bool operator!= (const qnMtx3& lhs, const qnMtx3& rhs) { return !(lhs == rhs); }
} // namespace gml

#endif // GML_MTX3_H
