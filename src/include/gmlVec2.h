#ifndef GML_VEC2_H
#define GML_VEC2_H
#pragma once

#include "gmlMath.h"

namespace gml {
	class qnMtx2;

	class GML_EXPORT qnVec2
	{
	public:
		qnVec2() {}; // No default init!
		qnVec2(const float _x, const float _y);
		qnVec2(const qnVec2& rhs);
		qnVec2& operator= (const qnVec2& rhs);

		float Magnitude() const;
		float MagnitudeS() const; // Magnitude squared

		qnVec2& Normalize();
		qnVec2& NormalizeCopy(qnVec2& to) const;

		// Member versions, work in place.
		qnVec2& Add(const qnVec2& rhs);
		qnVec2& Sub(const qnVec2& rhs);
		qnVec2& Mul(const float rhs);
		qnVec2& Mul(const qnMtx2& rhs);
		float Dot(const qnVec2& rhs) const;

		qnVec2& Stabelize();

		// Non-member
		static qnVec2 Add(const qnVec2& lhs, const qnVec2& rhs);
		static qnVec2 Sub(const qnVec2& lhs, const qnVec2& rhs);
		static qnVec2 Mul(const qnVec2& lhs, const float rhs);
		static qnVec2 Mul(const qnVec2& lhs, const qnMtx2& rhs);
		static float Dot(const qnVec2& lhs, const qnVec2& rhs);

		static qnVec2 Stabelize(const qnVec2& v);

		// Data
		float x, y;

		static const qnVec2 ZERO;
		static const qnVec2 ONE;
		static const qnVec2 X_AXIS;
		static const qnVec2 Y_AXIS;
	};

	// Binary operators
	inline qnVec2 operator+ (const qnVec2& lhs, const qnVec2& rhs) { return qnVec2::Add(lhs, rhs); }
	inline qnVec2 operator- (const qnVec2& lhs, const qnVec2& rhs) { return qnVec2::Sub(lhs, rhs); }
	inline qnVec2 operator* (const qnVec2& lhs, const float s) { return qnVec2::Mul(lhs, s); }
	inline qnVec2 operator* (const float s, const qnVec2& rhs) { return qnVec2::Mul(rhs, s); }
	inline float operator* (const qnVec2& lhs, const qnVec2& rhs) { return qnVec2::Dot(lhs, rhs); }
	inline qnVec2 operator* (const qnVec2& lhs, const qnMtx2& rhs) { return qnVec2::Mul(lhs, rhs); }

	inline qnVec2& operator+= (qnVec2& lhs, const qnVec2& rhs) { return lhs.Add(rhs); }
	inline qnVec2& operator-= (qnVec2& lhs, const qnVec2& rhs) { return lhs.Sub(rhs); }
	inline qnVec2& operator*= (qnVec2& lhs, const float s) { return lhs.Mul(s); }
	inline qnVec2& operator*= (qnVec2& lhs, const qnMtx2& rhs) { return lhs.Mul(rhs); }

	inline bool operator==(const qnVec2& lhs, const qnVec2& rhs) { return fcmp(lhs.x, rhs.x) && fcmp(lhs.y, rhs.y); }
	inline bool operator!=(const qnVec2& lhs, const qnVec2& rhs) { return !(lhs == rhs); }
}

#endif // GML_VEC2_H
