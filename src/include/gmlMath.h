#ifndef GML_MATH_H
#define GML_MATH_H

#include <cmath>
#include <limits>

#ifdef GAMEMATHLIBRARY_EXPORTS
#define GML_EXPORT __declspec(dllexport)
#else
#define GML_EXPORT __declspec(dllimport)
#endif

namespace gml
{
	constexpr float Pi{ 3.14159265f };
	constexpr float GML_EPSILON_F{ std::numeric_limits<float>::epsilon() };
	constexpr double GML_EPSILON_D{ std::numeric_limits<double>::epsilon() };

	inline
		float deg_to_rads(float deg)
	{
		deg = deg - (floor(deg / 360.f) * 360.f);
		return deg * gml::Pi / 180;
	}

	inline
		float rads_to_deg(float rads)
	{
		float tau = 2 * gml::Pi;
		return rads - (floor(rads / tau) * tau);
	}

	inline bool fcmp(float a, float b) { return (fabs(a - b) < GML_EPSILON_F); }
	inline bool dcmp(double a, double b) { return (fabs(a - b) < GML_EPSILON_D); }
}

#endif // GML_MATH_H
