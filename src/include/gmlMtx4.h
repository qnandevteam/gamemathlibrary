#ifndef DN_MATRIX_H
#define DN_MATRIX_H
#pragma once

#include "gmlMath.h"

namespace gml
{
	class qnVec3;
	class qnVec4;
	class qnQuat;

	class GML_EXPORT qnMtx4
	{
	public:
		qnMtx4() {} // No default initialization
		qnMtx4(float a, float b, float c, float d,
			float e, float f, float g, float h,
			float i, float j, float k, float l,
			float m, float n, float o, float p);
		qnMtx4(const qnMtx4& rhs);
		qnMtx4& operator= (const qnMtx4& rhs);

		// Members work in place
		qnMtx4& Scale(const float f); // Scalar multiply
		qnMtx4& Translate(const qnVec3& rhs);

		qnMtx4& Add(const qnMtx4& rhs);
		qnMtx4& Sub(const qnMtx4& rhs);
		qnMtx4& Mul(const qnMtx4& rhs);
		qnMtx4& Transpose();
		qnMtx4& Invert();

		qnMtx4& Stabelize();

		// non Member
		static qnMtx4 Scale(const qnMtx4& mtx, const float f);
		static qnMtx4 Translate(const qnMtx4& mtx, const qnVec3& vec);

		static qnMtx4 Add(const qnMtx4& lhs, const qnMtx4& rhs);
		static qnMtx4 Sub(const qnMtx4& lhs, const qnMtx4& rhs);
		static qnMtx4 Mul(const qnMtx4& lhs, const qnMtx4& rhs);
		static qnMtx4 Transpose(const qnMtx4& mtx);
		static qnMtx4 Invert(const qnMtx4& mtx);

		static qnMtx4 RotateX(float angle);
		static qnMtx4 RotateY(float angle);
		static qnMtx4 RotateZ(float angle);

		static qnMtx4 RotateEuler(float yaw, float pitch, float roll);
		static qnMtx4 RotateAxisAngle(qnVec3 axis, const float angle);
		static qnMtx4 RotateAxisAngle(qnVec4 axis, const float angle);
		static qnMtx4 RotateQuaternion(const qnQuat& quaternion);

		static void GetAxisAngle(const qnMtx4&, qnVec3* pAxis, float* pAngle);
		static void GetAxisAngle(const qnMtx4&, qnVec4* pAxis, float* pAngle);

		static qnMtx4 Stabelize(const qnMtx4& mtx);

		// Data, row major
		union {
			struct {
				float _00, _01, _02, _03,
					_10, _11, _12, _13,
					_20, _21, _22, _23,
					_30, _31, _32, _33;
			} mtx;
			float a[4][4];
			float f[16];
		} data;

		static const qnMtx4 ZERO;
		static const qnMtx4 FLIP_X;
		static const qnMtx4 FLIP_Y;
		static const qnMtx4 FLIP_Z;
		static const qnMtx4 FLIP_W;
		static const qnMtx4 IDENTITY;
	};

	inline qnMtx4 operator+ (const qnMtx4& lhs, const qnMtx4& rhs) { return qnMtx4::Add(lhs, rhs); }
	inline qnMtx4 operator- (const qnMtx4& lhs, const qnMtx4& rhs) { return qnMtx4::Sub(lhs, rhs); }
	inline qnMtx4 operator* (const float scale, const qnMtx4& rhs) { return qnMtx4::Scale(rhs, scale); }
	inline qnMtx4 operator* (const qnMtx4& lhs, const float scale) { return qnMtx4::Scale(lhs, scale); }
	inline qnMtx4 operator* (const qnMtx4& lhs, const qnMtx4& rhs) { return qnMtx4::Mul(lhs, rhs); }

	inline qnMtx4& operator+= (qnMtx4& lhs, const qnMtx4& rhs) { return lhs.Add(rhs); }
	inline qnMtx4& operator-= (qnMtx4& lhs, const qnMtx4& rhs) { return lhs.Sub(rhs); }
	inline qnMtx4& operator*= (qnMtx4& lhs, const float scale) { return lhs.Scale(scale); }
	inline qnMtx4& operator*= (qnMtx4& lhs, const qnMtx4& rhs) { return lhs.Mul(rhs); }

	bool GML_EXPORT operator== (const qnMtx4& lhs, const qnMtx4& rhs);
	inline bool operator!= (const qnMtx4& lhs, const qnMtx4& rhs) { return !(lhs == rhs); }
} // namespace gml

#endif // DN_MATRIX_H
