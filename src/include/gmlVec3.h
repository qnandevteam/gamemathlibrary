#ifndef GML_VEC3_H
#define GML_VEC3_H
#pragma once

#include "gmlMath.h"

namespace gml
{
	class qnMtx3;

	class GML_EXPORT qnVec3
	{
	public:
		qnVec3() {}; // No default init!
		qnVec3(const float _x, const float _y, const float _z);
		qnVec3(const qnVec3& rhs);
		qnVec3& operator= (const qnVec3& rhs);

		float Magnitude() const;
		float MagnitudeS() const; // Like magnitude, but does not sqrt

		qnVec3& Normalize();
		qnVec3& NormalizeCopy(qnVec3& to) const;

		// Member versions work "in place"
		qnVec3& Add(const qnVec3& rhs);
		qnVec3& Sub(const qnVec3& rhs);
		qnVec3& Mul(const float rhs);
		qnVec3& Mul(const qnMtx3& rhs);
		float Dot(const qnVec3& rhs) const;
		qnVec3& Cross(const qnVec3& rhs);

		qnVec3& Stabelize();

		// Non-member
		static qnVec3 Add(const qnVec3& lhs, const qnVec3& rhs);
		static qnVec3 Sub(const qnVec3& lhs, const qnVec3& rhs);
		static qnVec3 Mul(const float lhs, const qnVec3& rhs);
		static qnVec3 Mul(const qnVec3& lhs, const float rhs);
		static qnVec3 Mul(const qnVec3& lhs, const qnMtx3& rhs);
		static float Dot(const qnVec3& lhs, const qnVec3& rhs);
		static qnVec3 Cross(const qnVec3& lhs, const qnVec3& rhs);

		static qnVec3 Stabelize(const qnVec3& v);

		// Data
		float x, y, z;

		static const qnVec3 ZERO; // { 0, 0, 0 }
		static const qnVec3 ONE; // { 1, 1, 1 }
		static const qnVec3 X_AXIS; // { 1, 0, 0 }
		static const qnVec3 Y_AXIS; // { 0, 1, 0 }
		static const qnVec3 Z_AXIS; // { 0, 0, 1 }
	};

	inline qnVec3 operator+ (const qnVec3& lhs, const qnVec3& rhs) { return qnVec3::Add(lhs, rhs); }
	inline qnVec3 operator- (const qnVec3& lhs, const qnVec3& rhs) { return qnVec3::Sub(lhs, rhs); }
	inline qnVec3 operator* (const qnVec3& lhs, const float rhs) { return qnVec3::Mul(lhs, rhs); }
	inline qnVec3 operator* (const float lhs, const qnVec3& rhs) { return qnVec3::Mul(rhs, lhs); }
	inline qnVec3 operator* (const qnVec3& lhs, const qnMtx3& rhs) { return qnVec3::Mul(lhs, rhs); }
	inline float operator* (const qnVec3& lhs, const qnVec3& rhs) { return qnVec3::Dot(lhs, rhs); }

	inline qnVec3& operator+= (qnVec3& lhs, const qnVec3& rhs) { return lhs.Add(rhs); }
	inline qnVec3& operator-= (qnVec3& lhs, const qnVec3& rhs) { return lhs.Sub(rhs); }
	inline qnVec3& operator*= (qnVec3& lhs, const float rhs) { return lhs.Mul(rhs); }
	inline qnVec3& operator*= (qnVec3& lhs, const qnMtx3& rhs) { return lhs.Mul(rhs); }

	inline bool operator==(const qnVec3& lhs, const qnVec3& rhs) {
		return fcmp(lhs.x, rhs.x) && fcmp(lhs.y, rhs.y) && fcmp(lhs.z, rhs.z);
	}
	inline bool operator!=(const qnVec3& lhs, const qnVec3& rhs) { return !(lhs == rhs); }
}

#endif // GML_VEC3_H
