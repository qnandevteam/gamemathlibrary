#ifndef GML_VEC4_H
#define GML_VEC4_H
#pragma once

#include "gmlMath.h"

namespace gml
{
	class qnMtx4;

	class GML_EXPORT qnVec4
	{
	public:
		qnVec4() {}; // No default init!
		qnVec4(const float _x, const float _y, const float _z, const float _w);
		qnVec4(const qnVec4& rhs);
		qnVec4& operator= (const qnVec4& rhs);

		float Magnitude() const;
		float MagnitudeS() const; // Like magnitude, but does not sqrt

		qnVec4& Normalize();
		qnVec4& NormalizeCopy(qnVec4& to) const;

		// Member versions work "in place"
		qnVec4& Add(const qnVec4& rhs);
		qnVec4& Sub(const qnVec4& rhs);
		qnVec4& Mul(const float rhs);
		qnVec4& Mul(const qnMtx4& rhs);
		float Dot(const qnVec4& rhs) const;

		qnVec4& Stabelize();

		// Non-member
		static qnVec4 Add(const qnVec4& lhs, const qnVec4& rhs);
		static qnVec4 Sub(const qnVec4& lhs, const qnVec4& rhs);
		static qnVec4 Mul(const float lhs, const qnVec4& rhs);
		static qnVec4 Mul(const qnVec4& lhs, const float rhs);
		static qnVec4 Mul(const qnVec4& lhs, const qnMtx4& rhs);
		static float Dot(const qnVec4& lhs, const qnVec4& rhs);

		static qnVec4 Stabelize(const qnVec4& v);

		// Data
		float x, y, z, w;

		static const qnVec4 ZERO; // { 0, 0, 0, 0 }
		static const qnVec4 ONE; // { 1, 1, 1, 1 }
		static const qnVec4 X_AXIS; // { 1, 0, 0, 0 }
		static const qnVec4 Y_AXIS; // { 0, 1, 0, 0 }
		static const qnVec4 Z_AXIS; // { 0, 0, 1, 0 }
		static const qnVec4 W_AXIS; // { 0, 0, 0, 1 }
	};

	inline qnVec4 operator+ (const qnVec4& lhs, const qnVec4& rhs) { return qnVec4::Add(lhs, rhs); }
	inline qnVec4 operator- (const qnVec4& lhs, const qnVec4& rhs) { return qnVec4::Sub(lhs, rhs); }
	inline qnVec4 operator* (const qnVec4& lhs, const float rhs) { return qnVec4::Mul(lhs, rhs); }
	inline qnVec4 operator* (const float lhs, const qnVec4& rhs) { return qnVec4::Mul(rhs, lhs); }
	inline qnVec4 operator* (const qnVec4& lhs, const qnMtx4& rhs) { return qnVec4::Mul(lhs, rhs); }
	inline float operator* (const qnVec4& lhs, const qnVec4& rhs) { return qnVec4::Dot(lhs, rhs); }

	inline qnVec4& operator+= (qnVec4& lhs, const qnVec4& rhs) { return lhs.Add(rhs); }
	inline qnVec4& operator-= (qnVec4& lhs, const qnVec4& rhs) { return lhs.Sub(rhs); }
	inline qnVec4& operator*= (qnVec4& lhs, const float rhs) { return lhs.Mul(rhs); }
	inline qnVec4& operator*= (qnVec4& lhs, const qnMtx4& rhs) { return lhs.Mul(rhs); }

	inline bool operator==(const qnVec4& lhs, const qnVec4& rhs) {
		return fcmp(lhs.x, rhs.x) && fcmp(lhs.y, rhs.y) && fcmp(lhs.z, rhs.z);
	}
	inline bool operator!=(const qnVec4& lhs, const qnVec4& rhs) { return !(lhs == rhs); }
} // namespace gml

#endif // GML_VEC4_H
