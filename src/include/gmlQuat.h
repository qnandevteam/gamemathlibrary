#ifndef DN_QUAT_H
#define DN_QUAT_H
#pragma once

#include "gmlMath.h"
#include "gmlMtx4.h"
#include "gmlVec3.h"
#include "gmlVec4.h"

namespace gml
{
	class GML_EXPORT qnQuat
	{
	public:
		qnQuat();
		qnQuat(const float _w, const float _x, const float _y, const float _z); // Direct init
		qnQuat(const float yaw, const float pitch, const float roll); // From euler angles
		qnQuat(const float angle, const qnVec3& v); // from vector & angle
		qnQuat(const qnQuat& rhs); // copy
		qnQuat& operator= (const qnQuat& rhs); // assign

		float Magnitude() const;
		float MagnitudeS() const;
		qnQuat& Normalize();
		qnQuat& Add(const qnQuat& rhs);
		qnQuat& Sub(const qnQuat& rhs);
		qnQuat& Mul(const qnQuat& rhs);
		qnQuat& Mul(const float scalar);
		qnQuat& Invert();
		qnQuat Conjugate() const; // Assumes normalized
		qnVec3 RotatePt(const qnVec3& Pt) const;
		qnVec4 RotatePt(const qnVec4& Pt) const;
		void TransformPoints(qnVec3* points, unsigned count) const;
		void TransformPoints(qnVec4* points, unsigned count) const;

		qnMtx4 GetMatrix() const;
		void ToAxisAngle(qnVec3* axis, float* angle) const;

		static float Magnitude(const qnQuat& q) { return q.Magnitude(); }
		static float MagnitudeS(const qnQuat& q) { return q.MagnitudeS(); }
		static qnQuat Normalize(const qnQuat& q) { return qnQuat(q).Normalize(); }
		static qnQuat Add(const qnQuat& lhs, const qnQuat& rhs) { return qnQuat(lhs).Add(rhs); }
		static qnQuat Sub(const qnQuat& lhs, const qnQuat& rhs) { return qnQuat(lhs).Sub(rhs); }
		static qnQuat Mul(const qnQuat& lhs, const qnQuat& rhs) { return qnQuat(lhs).Mul(rhs); }
		static qnQuat Mul(const qnQuat& lhs, const float scalar) { return qnQuat(lhs).Mul(scalar); }
		static qnQuat Conjugate(const qnQuat& lhs) { return lhs.Conjugate(); }
		static qnQuat Invert(const qnQuat& lhs) { return qnQuat(lhs).Invert(); }
		static qnVec3 RotatePt(const qnQuat& lhs, const qnVec3& pt) { return lhs.RotatePt(pt); }
		static qnVec4 RotatePt(const qnQuat& lhs, const qnVec4& pt) { return lhs.RotatePt(pt); }
		static void TransformPoints(const qnQuat& lhs, qnVec3* points, unsigned count) { lhs.TransformPoints(points, count); }
		static void TransformPoints(const qnQuat& lhs, qnVec4* points, unsigned count) { lhs.TransformPoints(points, count); }

		static qnMtx4 GetMatrix(const qnQuat& lhs) { return lhs.GetMatrix(); }
		static void ToAxisAngle(const qnQuat& q, qnVec3* axis, float* angle) { return q.ToAxisAngle(axis, angle); }

		static qnQuat Lerp(const qnQuat& start, const qnQuat& end, float alpha);
		static qnQuat Slerp(const qnQuat& start, const qnQuat& end, float alpha);

		float w, x, y, z;
	};

	inline qnQuat operator + (const qnQuat& lhs, const qnQuat& rhs) { return qnQuat::Add(lhs, rhs); }
	inline qnQuat operator - (const qnQuat& lhs, const qnQuat& rhs) { return qnQuat::Sub(lhs, rhs); }
	inline qnQuat operator * (const qnQuat& lhs, const qnQuat& rhs) { return qnQuat::Mul(lhs, rhs); }
	inline qnQuat operator * (const qnQuat& lhs, const float scalar) { return qnQuat::Mul(lhs, scalar); }
	inline qnVec3 operator * (const qnQuat& lhs, const qnVec3& rhs) { return qnQuat::RotatePt(lhs, rhs); }
	inline qnVec4 operator * (const qnQuat& lhs, const qnVec4& rhs) { return qnQuat::RotatePt(lhs, rhs); }
	inline qnQuat operator ~ (const qnQuat& rhs) { return rhs.Conjugate(); }

	inline bool operator == (const qnQuat& lhs, const qnQuat& rhs) {
		return fcmp(lhs.x, rhs.x) && fcmp(lhs.y, rhs.y) && fcmp(lhs.z, rhs.z);
	}
	inline bool operator != (const qnQuat& lhs, const qnQuat& rhs) { return !(lhs == rhs); }

	inline qnQuat& operator += (qnQuat& lhs, const qnQuat& rhs) { return lhs.Add(rhs); }
	inline qnQuat& operator -= (qnQuat& lhs, const qnQuat& rhs) { return lhs.Sub(rhs); }
	inline qnQuat& operator *= (qnQuat& lhs, const qnQuat& rhs) { return lhs.Mul(rhs); }
	inline qnQuat& operator *= (qnQuat& lhs, const float rhs) { return lhs.Mul(rhs); }
}

#endif // DN_QUAT_H
