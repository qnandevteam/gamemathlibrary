#ifndef GML_MTX2_H
#define GML_MTX2_H
#pragma once

#include "gmlMath.h"

namespace gml {
	class GML_EXPORT qnMtx2
	{
	public:
		qnMtx2() {} // No default initialization
		qnMtx2(float a, float b, float c, float d);
		qnMtx2(const qnMtx2& rhs);
		qnMtx2& operator= (const qnMtx2& rhs);

		// Members work in place
		qnMtx2& Scale(const float f); // Scalar multiply

		qnMtx2& Add(const qnMtx2& rhs);
		qnMtx2& Sub(const qnMtx2& rhs);
		qnMtx2& Mul(const qnMtx2& rhs);
		qnMtx2& Transpose();
		qnMtx2& Invert();

		qnMtx2& Stabelize();

		// non Member
		static qnMtx2 Scale(const qnMtx2& mtx, const float f);

		static qnMtx2 Add(const qnMtx2& lhs, const qnMtx2& rhs);
		static qnMtx2 Sub(const qnMtx2& lhs, const qnMtx2& rhs);
		static qnMtx2 Mul(const qnMtx2& lhs, const qnMtx2& rhs);
		static qnMtx2 Transpose(const qnMtx2& mtx);
		static qnMtx2 Invert(const qnMtx2& mtx);

		static qnMtx2 Rotation(float angle);

		static qnMtx2 Stabelize(const qnMtx2& mtx);

		union
		{
			struct {
				float _00, _01,
					_10, _11;
			} mtx;
			float a[2][2];
			float f[4];
		} data;

		static const qnMtx2 ZERO;
		static const qnMtx2 ONE;
		static const qnMtx2 FLIP_X;
		static const qnMtx2 FLIP_Y;
		static const qnMtx2 IDENTITY;
	};

	inline qnMtx2 operator+ (const qnMtx2& lhs, const qnMtx2& rhs) { return qnMtx2::Add(lhs, rhs); }
	inline qnMtx2 operator- (const qnMtx2& lhs, const qnMtx2& rhs) { return qnMtx2::Sub(lhs, rhs); }
	inline qnMtx2 operator* (const float scale, const qnMtx2& rhs) { return qnMtx2::Scale(rhs, scale); }
	inline qnMtx2 operator* (const qnMtx2& lhs, const float scale) { return qnMtx2::Scale(lhs, scale); }
	inline qnMtx2 operator* (const qnMtx2& lhs, const qnMtx2& rhs) { return qnMtx2::Mul(lhs, rhs); }

	inline qnMtx2& operator+= (qnMtx2& lhs, const qnMtx2& rhs) { return lhs.Add(rhs); }
	inline qnMtx2& operator-= (qnMtx2& lhs, const qnMtx2& rhs) { return lhs.Sub(rhs); }
	inline qnMtx2& operator*= (qnMtx2& lhs, const float scale) { return lhs.Scale(scale); }
	inline qnMtx2& operator*= (qnMtx2& lhs, const qnMtx2& rhs) { return lhs.Mul(rhs); }

	bool GML_EXPORT operator== (const qnMtx2& lhs, const qnMtx2& rhs);
	inline bool operator!= (const qnMtx2& lhs, const qnMtx2& rhs) { return !(lhs == rhs); }
}

#endif // GML_MTX2_H
