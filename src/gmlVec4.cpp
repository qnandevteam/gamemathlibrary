#include "gmlVec4.h"

#include "gmlMtx4.h"

#include <math.h>

namespace gml
{
	const qnVec4 qnVec4::ZERO = qnVec4(0, 0, 0, 0);
	const qnVec4 qnVec4::ONE = qnVec4(1, 1, 1, 1);
	const qnVec4 qnVec4::X_AXIS = qnVec4(1, 0, 0, 0);
	const qnVec4 qnVec4::Y_AXIS = qnVec4(0, 1, 0, 0);
	const qnVec4 qnVec4::Z_AXIS = qnVec4(0, 0, 1, 0);
	const qnVec4 qnVec4::W_AXIS = qnVec4(0, 0, 0, 1);

	qnVec4::qnVec4(const float _x, const float _y, const float _z, const float _w) :
		x(_x), y(_y), z(_z), w(_w)
	{
	}

	qnVec4::qnVec4(const qnVec4& rhs) :
		x(rhs.x), y(rhs.y), z(rhs.z), w(rhs.w)
	{
	}

	qnVec4& qnVec4::operator= (const qnVec4& rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		w = rhs.w;
		return *this;
	}


	float qnVec4::Magnitude() const
	{
		return sqrt(MagnitudeS());
	}

	float qnVec4::MagnitudeS() const // Like magnitude, but does not sqrt (equivalent to A.Dot(A))
	{
		return x * x + y * y + z * z + w * w;
	}

	qnVec4& qnVec4::Normalize()
	{
		if (float m = Magnitude())
		{
			x /= m;
			y /= m;
			z /= m;
			w /= m;
		}
		return *this;
	}

	qnVec4& qnVec4::NormalizeCopy(qnVec4& to) const
	{
		if (float m = Magnitude())
		{
			to.x = x / m;
			to.y = y / m;
			to.z = z / m;
			to.w = w / m;
		}
		return to;
	}


	// Member versions work "in place"
	qnVec4& qnVec4::Add(const qnVec4& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		w += rhs.w;
		return *this;
	}

	qnVec4& qnVec4::Sub(const qnVec4& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;
		w -= rhs.w;
		return *this;
	}

	qnVec4& qnVec4::Mul(const float rhs)
	{
		x *= rhs;
		y *= rhs;
		z *= rhs;
		w *= rhs;
		return *this;
	}

	qnVec4& qnVec4::Mul(const qnMtx4& rhs)
	{
		float xp = (x * rhs.data.mtx._00)
			+ (y * rhs.data.mtx._10)
			+ (z * rhs.data.mtx._20)
			+ (w * rhs.data.mtx._30);

		float yp = (x * rhs.data.mtx._01)
			+ (y * rhs.data.mtx._11)
			+ (z * rhs.data.mtx._21)
			+ (w * rhs.data.mtx._31);

		float zp = (x * rhs.data.mtx._02)
			+ (y * rhs.data.mtx._12)
			+ (z * rhs.data.mtx._22)
			+ (w * rhs.data.mtx._32);

		float wp = (x * rhs.data.mtx._03)
			+ (y * rhs.data.mtx._13)
			+ (z * rhs.data.mtx._23)
			+ (w * rhs.data.mtx._33);

		x = xp;
		y = yp;
		z = zp;
		w = wp;
		return *this;
	}

	float qnVec4::Dot(const qnVec4& rhs) const
	{
		return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w;
	}

	qnVec4& qnVec4::Stabelize()
	{
		if (fabs(x) < GML_EPSILON_F)
			x = 0;
		if (fabs(y) < GML_EPSILON_F)
			y = 0;
		if (fabs(z) < GML_EPSILON_F)
			z = 0;
		if (fabs(w) < GML_EPSILON_F)
			w = 0;
		return *this;
	}

	// Non-member
	qnVec4 qnVec4::Add(const qnVec4& lhs, const qnVec4& rhs)
	{
		return qnVec4(lhs).Add(rhs);
	}

	qnVec4 qnVec4::Sub(const qnVec4& lhs, const qnVec4& rhs)
	{
		return qnVec4(lhs).Sub(rhs);
	}

	qnVec4 qnVec4::Mul(const float lhs, const qnVec4& rhs)
	{
		return qnVec4(rhs).Mul(lhs);
	}

	qnVec4 qnVec4::Mul(const qnVec4& lhs, const float rhs)
	{
		return qnVec4(lhs).Mul(rhs);
	}

	qnVec4 qnVec4::Mul(const qnVec4& lhs, const qnMtx4& rhs)
	{
		return qnVec4(lhs).Mul(rhs);
	}

	float qnVec4::Dot(const qnVec4& lhs, const qnVec4& rhs)
	{
		return lhs.Dot(rhs);
	}

	qnVec4 qnVec4::Stabelize(const qnVec4& a)
	{
		return qnVec4(a).Stabelize();
	}
} // namespace gml
