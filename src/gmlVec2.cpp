#include "gmlVec2.h"

#include "gmlMath.h"
#include "gmlMtx2.h"

#include <cmath>

namespace gml {
	const qnVec2 qnVec2::ZERO = qnVec2(0, 0);
	const qnVec2 qnVec2::ONE = qnVec2(1, 1);
	const qnVec2 qnVec2::X_AXIS = qnVec2(1, 0);
	const qnVec2 qnVec2::Y_AXIS = qnVec2(0, 1);

	qnVec2::qnVec2(const float _x, const float _y) :x(_x), y(_y) {}
	qnVec2::qnVec2(const qnVec2& rhs) : x(rhs.x), y(rhs.y) {}
	qnVec2& qnVec2::operator= (const qnVec2& rhs)
	{
		x = rhs.x;
		y = rhs.y;
		return *this;
	}

	float qnVec2::Magnitude() const
	{
		return sqrt(MagnitudeS());
	}

	float qnVec2::MagnitudeS() const
	{
		return (x * x) + (y * y);
	}

	qnVec2& qnVec2::Normalize()
	{
		float mag = Magnitude();
		x /= mag;
		y /= mag;
		return *this;
	}

	qnVec2& qnVec2::NormalizeCopy(qnVec2& to) const
	{
		float mag = Magnitude();
		to.x = x / mag;
		to.y = y / mag;
		return to;
	}

	// Member versions, work in place.
	qnVec2& qnVec2::Add(const qnVec2& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	qnVec2& qnVec2::Sub(const qnVec2& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		return *this;
	}

	qnVec2& qnVec2::Mul(const float rhs)
	{
		x *= rhs;
		y *= rhs;
		return *this;
	}

	qnVec2& qnVec2::Mul(const qnMtx2& rhs)
	{
		*this = qnVec2::Mul(*this, rhs);
		return *this;
	}

	float qnVec2::Dot(const qnVec2& rhs) const
	{
		return (x * rhs.x) + (y * rhs.y);
	}

	qnVec2& qnVec2::Stabelize()
	{
		if (fabs(x) < GML_EPSILON_F)
			x = 0;
		if (fabs(y) < GML_EPSILON_F)
			y = 0;
		return *this;
	}

	// Non-meber
	qnVec2 qnVec2::Add(const qnVec2& lhs, const qnVec2& rhs)
	{
		return qnVec2(lhs).Add(rhs);
	}

	qnVec2 qnVec2::Sub(const qnVec2& lhs, const qnVec2& rhs)
	{
		return qnVec2(lhs).Sub(rhs);
	}

	qnVec2 qnVec2::Mul(const qnVec2& lhs, const float rhs)
	{
		return qnVec2(lhs).Mul(rhs);
	}

	qnVec2 qnVec2::Mul(const qnVec2& lhs, const qnMtx2& rhs)
	{
		return qnVec2(
			(lhs.x * rhs.data.mtx._00) + (lhs.y * rhs.data.mtx._10),
			(lhs.x * rhs.data.mtx._01) + (lhs.y * rhs.data.mtx._11));
	}

	float qnVec2::Dot(const qnVec2& lhs, const qnVec2& rhs)
	{
		return lhs.Dot(rhs);
	}

	qnVec2 qnVec2::Stabelize(const qnVec2& v)
	{
		return qnVec2(v).Stabelize();
	}
} // namespace gml
