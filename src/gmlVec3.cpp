#include "gmlVec3.h"

#include "gmlMtx3.h"

#include <math.h>

namespace gml
{
	const qnVec3 qnVec3::ZERO = qnVec3(0, 0, 0);
	const qnVec3 qnVec3::ONE = qnVec3(1, 1, 1);
	const qnVec3 qnVec3::X_AXIS = qnVec3(1, 0, 0);
	const qnVec3 qnVec3::Y_AXIS = qnVec3(0, 1, 0);
	const qnVec3 qnVec3::Z_AXIS = qnVec3(0, 0, 1);

	qnVec3::qnVec3(const float _x, const float _y, const float _z) :
		x(_x), y(_y), z(_z)
	{
	}

	qnVec3::qnVec3(const qnVec3& rhs) :
		x(rhs.x), y(rhs.y), z(rhs.z)
	{
	}

	qnVec3& qnVec3::operator= (const qnVec3& rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		return *this;
	}


	float qnVec3::Magnitude() const
	{
		return sqrt(MagnitudeS());
	}

	float qnVec3::MagnitudeS() const // Like magnitude, but does not sqrt (equivalent to A.Dot(A))
	{
		return x * x + y * y + z * z;
	}

	qnVec3& qnVec3::Normalize()
	{
		if (float m = Magnitude())
		{
			x /= m;
			y /= m;
			z /= m;
		}
		return *this;
	}

	qnVec3& qnVec3::NormalizeCopy(qnVec3& to) const
	{
		if (float m = Magnitude())
		{
			to.x = x / m;
			to.y = y / m;
			to.z = z / m;
		}
		return to;
	}


	// Member versions work "in place"
	qnVec3& qnVec3::Add(const qnVec3& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		return *this;
	}

	qnVec3& qnVec3::Sub(const qnVec3& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;
		return *this;
	}

	qnVec3& qnVec3::Mul(const float rhs)
	{
		x *= rhs;
		y *= rhs;
		z *= rhs;
		return *this;
	}

	qnVec3& qnVec3::Mul(const qnMtx3& rhs)
	{
		float xp = (x * rhs.data.mtx._00)
			+ (y * rhs.data.mtx._10)
			+ (z * rhs.data.mtx._20);

		float yp = (x * rhs.data.mtx._01)
			+ (y * rhs.data.mtx._11)
			+ (z * rhs.data.mtx._21);

		float zp = (x * rhs.data.mtx._02)
			+ (y * rhs.data.mtx._12)
			+ (z * rhs.data.mtx._22);

		x = xp;
		y = yp;
		z = zp;
		return *this;
	}

	float qnVec3::Dot(const qnVec3& rhs) const
	{
		return x * rhs.x + y * rhs.y + z * rhs.z;
	}

	qnVec3& qnVec3::Cross(const qnVec3& rhs)
	{
		float x_ = y * rhs.z - z * rhs.y;
		float y_ = z * rhs.x - x * rhs.z;
		float z_ = x * rhs.y - y * rhs.x;
		x = x_;
		y = y_;
		z = z_;
		return *this;
	}

	qnVec3& qnVec3::Stabelize()
	{
		if (fabs(x) < GML_EPSILON_F)
			x = 0;
		if (fabs(y) < GML_EPSILON_F)
			y = 0;
		if (fabs(z) < GML_EPSILON_F)
			z = 0;
		return *this;
	}

	// Non-member
	qnVec3 qnVec3::Add(const qnVec3& lhs, const qnVec3& rhs)
	{
		return qnVec3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
	}

	qnVec3 qnVec3::Sub(const qnVec3& lhs, const qnVec3& rhs)
	{
		return qnVec3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
	}

	qnVec3 qnVec3::Mul(const float lhs, const qnVec3& rhs)
	{
		return qnVec3(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
	}

	qnVec3 qnVec3::Mul(const qnVec3& lhs, const float rhs)
	{
		return qnVec3(rhs * lhs.x, rhs * lhs.y, rhs * lhs.z);
	}

	qnVec3 qnVec3::Mul(const qnVec3& lhs, const qnMtx3& rhs)
	{
		return qnVec3(lhs).Mul(rhs);
	}

	float qnVec3::Dot(const qnVec3& lhs, const qnVec3& rhs)
	{
		return lhs.Dot(rhs);
	}

	qnVec3 qnVec3::Cross(const qnVec3& lhs, const qnVec3& rhs)
	{
		return qnVec3(
			lhs.y * rhs.z - lhs.z * rhs.y,
			lhs.z * rhs.x - lhs.x * rhs.z,
			lhs.x * rhs.y - lhs.y * rhs.x
		);
	}

	qnVec3 qnVec3::Stabelize(const qnVec3& a)
	{
		return qnVec3(a).Stabelize();
	}
} // namespace gml
