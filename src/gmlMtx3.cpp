#include "gmlMtx3.h"

#include <cmath>
#include <memory>

#include "gmlVec3.h"
#include "gmlMtx4.h"
#include "gmlQuat.h"


namespace gml
{
	const qnMtx3 qnMtx3::ZERO = qnMtx3(0, 0, 0, 0, 0, 0, 0, 0, 0);
	const qnMtx3 qnMtx3::ONE = qnMtx3(1, 1, 1, 1, 1, 1, 1, 1, 1);
	const qnMtx3 qnMtx3::FLIP_X = qnMtx3(-1, 0, 0, 0, 1, 0, 0, 0, 1);
	const qnMtx3 qnMtx3::FLIP_Y = qnMtx3(1, 0, 0, 0, -1, 0, 0, 0, 1);
	const qnMtx3 qnMtx3::FLIP_Z = qnMtx3(1, 0, 0, 0, 1, 0, 0, 0, -1);
	const qnMtx3 qnMtx3::IDENTITY = qnMtx3(1, 0, 0, 0, 1, 0, 0, 0, 1);

	qnMtx3::qnMtx3(float a, float b, float c, float d, float e, float f, float g, float h, float i)
	{
		data.mtx._00 = a; data.mtx._01 = b; data.mtx._02 = c;
		data.mtx._10 = d; data.mtx._11 = e; data.mtx._12 = f;
		data.mtx._20 = g; data.mtx._21 = h; data.mtx._22 = i;
	}

	qnMtx3::qnMtx3(const qnMtx3& rhs)
	{
		for (unsigned n = 0; n < 9; ++n)
		{
			data.f[n] = rhs.data.f[n];
		}
	}

	qnMtx3& qnMtx3::operator= (const qnMtx3& rhs)
	{
		for (unsigned n = 0; n < 9; ++n)
		{
			data.f[n] = rhs.data.f[n];
		}
		return *this;
	}

	// Members work in place
	qnMtx3& qnMtx3::Scale(const float f)
	{
		for (unsigned n = 0; n < 9; ++n)
		{
			data.f[n] *= f;
		}
		return *this;
	}

	qnMtx3& qnMtx3::Add(const qnMtx3& rhs)
	{
		for (unsigned n = 0; n < 9; ++n)
		{
			data.f[n] += rhs.data.f[n];
		}
		return *this;
	}

	qnMtx3& qnMtx3::Sub(const qnMtx3& rhs)
	{
		for (unsigned n = 0; n < 9; ++n)
		{
			data.f[n] -= rhs.data.f[n];
		}
		return *this;
	}

	qnMtx3& qnMtx3::Mul(const qnMtx3& rhs)
	{
		float f[3];
		for (unsigned row = 0; row < 3; ++row)
		{
			memcpy(f, data.a[row], sizeof(float) * 3);
			for (unsigned n = 0; n < 3; ++n)
			{
				data.a[row][n] = f[0] * rhs.data.a[0][n] + f[1] * rhs.data.a[1][n] + f[2] * rhs.data.a[2][n];
			}
		}
		return *this;
	}

	qnMtx3& qnMtx3::Transpose()
	{
		// 01 x 10
		float tmp = data.mtx._01;
		data.mtx._01 = data.mtx._10;
		data.mtx._10 = tmp;

		// 02 x 20
		tmp = data.mtx._02;
		data.mtx._02 = data.mtx._20;
		data.mtx._20 = tmp;

		// 12 x 21
		tmp = data.mtx._12;
		data.mtx._12 = data.mtx._21;
		data.mtx._21 = tmp;

		return *this;
	}

	qnMtx3& qnMtx3::Invert()
	{
		return (*this = qnMtx3::Invert(*this));
	}

	qnMtx3& qnMtx3::Stabelize()
	{
		for (unsigned n = 0; n < 9; ++n)
		{
			if (fabs(data.f[n]) < GML_EPSILON_F)
				data.f[n] = 0;
		}
		return *this;
	}

	// non Member
	qnMtx3 qnMtx3::Scale(const qnMtx3& mtx, const float f)
	{
		return qnMtx3(mtx).Scale(f);
	}

	qnMtx3 qnMtx3::Add(const qnMtx3& lhs, const qnMtx3& rhs)
	{
		return qnMtx3(lhs).Add(rhs);
	}

	qnMtx3 qnMtx3::Sub(const qnMtx3& lhs, const qnMtx3& rhs)
	{
		return qnMtx3(lhs).Sub(rhs);
	}

	qnMtx3 qnMtx3::Mul(const qnMtx3& lhs, const qnMtx3& rhs)
	{
		return qnMtx3(lhs).Mul(rhs);
	}

	qnMtx3 qnMtx3::Transpose(const qnMtx3& mtx)
	{
		return qnMtx3(mtx).Transpose();
	}

	qnMtx3 qnMtx3::Invert(const qnMtx3& mtx)
	{
		auto& m = mtx.data.mtx;
		float det = m._00 * ((m._11 * m._22) - (m._12 * m._21))
			- m._01 * ((m._10 * m._22) - (m._12 * m._20))
			+ m._02 * ((m._10 * m._21) - (m._11 * m._20));
		float det_recip = 1.f / det;
		if (det)
		{
			qnMtx3 result;
			float* f = result.data.f;
			f[0] = (m._11 * m._22 - m._12 * m._21);
			f[1] = -(m._01 * m._22 - m._02 * m._21);
			f[2] = (m._01 * m._12 - m._02 * m._11);
			f[3] = -(m._10 * m._22 - m._12 * m._20);
			f[4] = (m._00 * m._22 - m._02 * m._20);
			f[5] = -(m._00 * m._12 - m._10 * m._02);
			f[6] = (m._10 * m._21 - m._20 * m._11);
			f[7] = -(m._00 * m._21 - m._20 * m._01);
			f[8] = (m._00 * m._11 - m._10 * m._01);
			for (unsigned n = 0; n < 9; ++n)
				f[n] *= det_recip;
			return result;
		}
		return qnMtx3::ZERO;
	}

	qnMtx3 qnMtx3::RotateX(float angle)
	{
		return qnMtx3(1, 0, 0,
			0, cos(angle), sin(angle),
			0, -sin(angle), cos(angle));
	}

	qnMtx3 qnMtx3::RotateY(float angle)
	{
		return qnMtx3(cos(angle), 0, -sin(angle),
			0, 1, 0,
			sin(angle), 0, cos(angle));
	}

	qnMtx3 qnMtx3::RotateZ(float angle)
	{
		return qnMtx3(cos(angle), sin(angle), 0,
			-sin(angle), cos(angle), 0,
			0, 0, 1);
	}

	qnMtx3 qnMtx3::RotateAxisAngle(qnVec3 v, const float angle)
	{
		float sina = sin(angle);
		float cosa = cos(angle);
		float cosa_1 = 1 - cosa;
		v.Normalize();
		float x2 = v.x * v.x * cosa_1, y2 = v.y * v.y * cosa_1, z2 = v.z * v.z * cosa_1;
		float xy = v.x * v.y * cosa_1, xz = v.x * v.z * cosa_1, yz = v.y * v.z * cosa_1;
		float xsin = v.x * sina, ysin = v.y * sina, zsin = v.z * sina;
		return qnMtx3(cosa + x2, xy + zsin, xz - ysin,
			xy - zsin, cosa + y2, yz + xsin,
			xz + ysin, yz - xsin, cosa + z2);
	}

	void qnMtx3::GetAxisAngle(const qnMtx3& mtx, qnVec3* pAxis, float* pAngle)
	{
		if (pAngle)
			*pAngle = acos((mtx.data.mtx._00 + mtx.data.mtx._11 + mtx.data.mtx._22 - 1) / 2);
		if (pAxis)
		{
			pAxis->x = mtx.data.mtx._12 - mtx.data.mtx._21;
			pAxis->y = mtx.data.mtx._20 - mtx.data.mtx._02;
			pAxis->z = mtx.data.mtx._01 - mtx.data.mtx._10;
			pAxis->Normalize();
		}
	}

	qnMtx3 qnMtx3::RotateEuler(float yaw, float pitch, float roll)
	{
		float cy = cos(yaw), sy = sin(yaw);
		float cp = cos(pitch), sp = sin(pitch);
		float cr = cos(roll), sr = sin(roll);

		return qnMtx3(cr * cy + sr * sp * sy, sr * cp, sr * sp * cy - cr * sy,
			cr * sp * sy - sr * cy, cr * cp, sr * sy + cr * sp * cy,
			cp * sy, -sp, cp * cy);
	}

	qnMtx3 qnMtx3::RotateQuaternion(const qnQuat& quat)
	{
		qnMtx4 m = quat.GetMatrix();
		return qnMtx3(m.data.mtx._00, m.data.mtx._01, m.data.mtx._02,
			m.data.mtx._10, m.data.mtx._11, m.data.mtx._12,
			m.data.mtx._20, m.data.mtx._21, m.data.mtx._22);
	}

	/*
	void qnMtx3::GetEulerAngles( const qnMtx3& mtx, float* yaw, float* pitch, float* roll )
	{
		mtx;
		if (yaw) *yaw = 0;
		if (pitch) *pitch = 0;
		if (roll) * roll = 0;
	}
	*/

	qnMtx3 qnMtx3::Stabelize(const qnMtx3& mtx)
	{
		return qnMtx3(mtx).Stabelize();
	}

	bool GML_EXPORT operator== (const qnMtx3& lhs, const qnMtx3& rhs)
	{
		for (unsigned n = 0; n < 9; ++n)
		{
			if (!fcmp(lhs.data.f[n], rhs.data.f[n]))
				return false;
		}
		return true;
	}
} // namespace gml
