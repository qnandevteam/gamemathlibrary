#include "gmlQuat.h"
#include "gmlVec3.h"
#include "gmlVec4.h"

#include "gmlMath.h"

#include <math.h>

namespace gml
{
	qnQuat::qnQuat() :w(1.f), x(0.f), y(0.f), z(0.f)
	{
	}

	qnQuat::qnQuat(float _w, float _x, float _y, float _z) :
		w(_w), x(_x), y(_y), z(_z)
	{
	}

	qnQuat::qnQuat(const float yaw, const float pitch, const float roll)
	{
		//const float PIOVER360 = ibMath::Pi / 360.f;
		//const float p = pitch * PIOVER360;
		//const float _y = yaw * PIOVER360;
		//const float r = roll * PIOVER360;

		//float sinp = sin(p), cosp = cos(p);
		//float siny = sin(_y), cosy = cos(_y);
		//float sinr = sin(r), cosr = cos(r);
		float sinp = sin(pitch / 2), cosp = cos(pitch / 2);
		float siny = sin(yaw / 2), cosy = cos(yaw / 2);
		float sinr = sin(roll / 2), cosr = cos(roll / 2);

		w = cosr * cosp * cosy + sinr * sinp * siny;
		x = cosr * sinp * cosy + sinr * cosp * siny;
		y = cosr * cosp * siny - sinr * sinp * cosy;
		z = sinr * cosp * cosy - cosr * sinp * siny;

		Normalize();
	}

	qnQuat::qnQuat(const float angle, const qnVec3& v)
	{
		w = cos(angle / 2);
		qnVec3 copy(v);
		copy.Normalize();
		float sin_a2 = sin(angle / 2);
		x = copy.x * sin_a2;
		y = copy.y * sin_a2;
		z = copy.z * sin_a2;
	}

	qnQuat::qnQuat(const qnQuat& rhs) :
		w(rhs.w), x(rhs.x), y(rhs.y), z(rhs.z)
	{
	}

	qnQuat& qnQuat::operator= (const qnQuat& rhs)
	{
		w = rhs.w;
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		return *this;
	}

	float qnQuat::Magnitude() const
	{
		return sqrt(w * w + x * x + y * y + z * z);
	}

	float qnQuat::MagnitudeS() const
	{
		return w * w + x * x + y * y + z * z;
	}

	qnQuat& qnQuat::Normalize()
	{
		float m = Magnitude();
		if (fabs(m) - 1.0f)
		{
			w /= m;
			x /= m;
			y /= m;
			z /= m;
		}
		return *this;
	}

	qnQuat& qnQuat::Add(const qnQuat& rhs)
	{
		w += rhs.w;
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		return *this;
	}

	qnQuat& qnQuat::Sub(const qnQuat& rhs)
	{
		w -= rhs.w;
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;
		return *this;
	}

	qnQuat& qnQuat::Mul(const qnQuat& rhs)
	{
		float tmps[4];
		tmps[0] = w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z;
		tmps[1] = w * rhs.x + x * rhs.w - y * rhs.z + z * rhs.y;
		tmps[2] = w * rhs.y + x * rhs.z + y * rhs.w - z * rhs.x;
		tmps[3] = w * rhs.z - x * rhs.y + y * rhs.x + z * rhs.w;
		w = tmps[0];
		x = tmps[1];
		y = tmps[2];
		z = tmps[3];
		return *this;
	}

	qnQuat& qnQuat::Mul(const float scalar)
	{
		w *= scalar;
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return *this;
	}

	qnQuat& qnQuat::Invert()
	{
		float length = 1.f / (x * x + y * y + z * z + w * w);
		w *= length;
		x *= -length;
		y *= -length;
		z *= -length;
		return *this;
	}

	qnQuat qnQuat::Conjugate() const
	{
		return qnQuat(w, -x, -y, -z);
	}

	qnMtx4 qnQuat::GetMatrix() const
	{
		float x2 = x * x;
		float y2 = y * y;
		float z2 = z * z;
		float xy = x * y;
		float xz = x * z;
		float yz = y * z;
		float wx = w * x;
		float wy = w * y;
		float wz = w * z;

		// This calculation would be a lot more complicated for non-unit length quaternions
		return qnMtx4(1.0f - 2.0f * (y2 + z2), 2.0f * (xy + wz), 2.0f * (xz - wy), 0.0f,
			2.0f * (xy - wz), 1.0f - 2.0f * (x2 + z2), 2.0f * (yz + wx), 0.0f,
			2.0f * (xz + wy), 2.0f * (yz - wx), 1.0f - 2.0f * (x2 + y2), 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}

	void qnQuat::ToAxisAngle(qnVec3* axis, float* angle) const
	{
		qnQuat qNorm(*this);
		qNorm.Normalize();

		float sinAngle = sqrt(1.0f - (qNorm.w * qNorm.w));
		if (fabs(sinAngle) < 0.0005f)
			sinAngle = 1.0f;

		if (axis)
		{
			axis->x = qNorm.x / sinAngle;
			axis->y = qNorm.y / sinAngle;
			axis->z = qNorm.z / sinAngle;
		}

		if (angle)
		{
			*angle = 2.f * acos(qNorm.w);
		}
	}

	qnVec3 qnQuat::RotatePt(const qnVec3& p) const
	{
		qnQuat res = Conjugate();
		(res *= qnQuat(0, p.x, p.y, p.z)) *= *this;
		qnVec3 v(res.x, res.y, res.z);
		return v;
	}

	qnVec4 qnQuat::RotatePt(const qnVec4& p) const
	{
		qnQuat res = Conjugate();
		(res *= qnQuat(0, p.x, p.y, p.z)) *= *this;
		qnVec4 v(res.x, res.y, res.z, p.w);
		return v;
	}

	void qnQuat::TransformPoints(qnVec3* points, unsigned count) const
	{
		for (unsigned n = 0; n < count; ++n)
			RotatePt(points[n]);
	}

	void qnQuat::TransformPoints(qnVec4* points, unsigned count) const
	{
		for (unsigned n = 0; n < count; ++n)
			RotatePt(points[n]);
	}

	/* static */
	qnQuat qnQuat::Lerp(const qnQuat& start, const qnQuat& end, float alpha)
	{
		return qnQuat(start * (1.0f - alpha) + end * alpha);
	}

#define SLERP_TO_LERP_SWITCH_THRESHOLD 0.01f

	/* static */
	qnQuat qnQuat::Slerp(const qnQuat& start, const qnQuat& end, float alpha)
	{
		float angle = start.x * end.x + start.y * end.y + start.z * end.z + start.w * end.w;
		float sign = 1.0f;
		if (angle < 0.0f)
		{
			angle = -angle;
			sign = -1.0f;
		}

		float startWeight = 1.f - alpha;
		float endWeight = alpha;

		if (angle <= 0.97f)
		{
			angle = acosf(angle);
			float oneOverSinTheta = 1.0f / sinf(angle);
			startWeight = sinf((1.0f - alpha) * angle) * oneOverSinTheta;
			endWeight = sinf(angle * alpha) * oneOverSinTheta;
		}

		startWeight *= sign;

		qnQuat result(
			(start.w * startWeight) + (end.w * endWeight),
			(start.x * startWeight) + (end.x * endWeight),
			(start.y * startWeight) + (end.y * endWeight),
			(start.z * startWeight) + (end.z * endWeight)
		);

		return result.Normalize();
	}

	/*
	qnQuat qnQuat::operator + ( const qnQuat & rhs ) const
	{
		return qnQuat( w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.z );
	}

	qnQuat qnQuat::operator - ( const qnQuat & rhs ) const
	{
		return qnQuat( w - rhs.w, x - rhs.x, y - rhs.y, z - rhs.z );
	}

	qnQuat qnQuat::operator * ( const qnQuat & rhs ) const
	{
		return qnQuat( *this ) *= rhs;
	}

	qnQuat qnQuat::operator / ( const qnQuat & rhs ) const
	{
		return (*this) * ~rhs;
	}

	qnQuat qnQuat::operator * ( const qnVec3 & rhs ) const
	{
		qnQuat tmp( 0.f, rhs.x, rhs.y, rhs.z );
		return qnQuat( *this ) *= tmp;
	}

	qnQuat qnQuat::operator * ( const float & rhs ) const
	{
		return qnQuat( w * rhs, x * rhs, y * rhs, z * rhs );
	}

	qnQuat qnQuat::operator / ( const float & rhs ) const
	{
		return qnQuat( w / rhs, x / rhs, y / rhs, z / rhs );
	}

	qnQuat qnQuat::operator~() const
	{
		return GetConjugate();
	}

	bool qnQuat::operator == ( const qnQuat & rhs ) const
	{
		return ((w == rhs.w) && (x == rhs.x) && (y == rhs.y) && (z == rhs.z));
	}

	bool qnQuat::operator != ( const qnQuat & rhs ) const
	{
		return !(*this == rhs);
	}

	qnQuat& qnQuat::operator += ( const qnQuat & rhs )
	{
		w += rhs.w;
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		return *this;
	}

	qnQuat& qnQuat::operator -= ( const qnQuat & rhs )
	{
		w -= rhs.w;
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;
		return *this;
	}

	qnQuat& qnQuat::operator *= ( const qnQuat & rhs )
	{
		return Mul( rhs );
	}

	qnQuat& qnQuat::operator *= ( const float & rhs )
	{
		w *= rhs;
		x *= rhs;
		y *= rhs;
		z *= rhs;
		return *this;
	}

	qnQuat& qnQuat::operator /= ( const float & rhs )
	{
		w /= rhs;
		x /= rhs;
		y /= rhs;
		z /= rhs;
		return *this;
	}
	*/
}
