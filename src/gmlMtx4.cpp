#include "gmlMtx4.h"
#include "gmlVec3.h"
#include "gmlVec4.h"
#include "gmlQuat.h"

#include <memory>
#include <emmintrin.h>

namespace gml
{
	namespace
	{
		void fswap(float& l, float& r)
		{
			float tmp = l;
			l = r;
			r = tmp;
		}
	}

	const qnMtx4 qnMtx4::ZERO = qnMtx4(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	const qnMtx4 qnMtx4::IDENTITY = qnMtx4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

	qnMtx4::qnMtx4(
		float a, float b, float c, float d,
		float e, float f, float g, float h,
		float i, float j, float k, float l,
		float m, float n, float o, float p)
	{
		float* pf = data.a[0];
		*pf++ = a; *pf++ = b; *pf++ = c; *pf++ = d;
		*pf++ = e; *pf++ = f; *pf++ = g; *pf++ = h;
		*pf++ = i; *pf++ = j; *pf++ = k; *pf++ = l;
		*pf++ = m; *pf++ = n; *pf++ = o; *pf++ = p;
	}

	qnMtx4::qnMtx4(const qnMtx4& rhs)
	{
		float* to = data.a[0];
		const float* from = rhs.data.a[0];
		for (unsigned n = 0; n < 16; ++n)
			*to++ = *from++;
	}

	qnMtx4& qnMtx4::operator= (const qnMtx4& rhs)
	{
		float* to = data.a[0];
		const float* from = rhs.data.a[0];
		for (unsigned n = 0; n < 16; ++n)
			*to++ = *from++;
		return *this;
	}

	// Members work in place
	qnMtx4& qnMtx4::Scale(const float f) // Scalar multiply
	{
		float* to = data.a[0];
		for (unsigned n = 0; n < 16; ++n)
			*to++ *= f;
		return *this;
	}

	qnMtx4& qnMtx4::Translate(const qnVec3& rhs)
	{
		data.mtx._03 += rhs.x;
		data.mtx._13 += rhs.y;
		data.mtx._23 += rhs.z;
		return *this;
	}

	qnMtx4& qnMtx4::Add(const qnMtx4& rhs)
	{
		float* to = data.a[0];
		const float* from = rhs.data.a[0];
		for (unsigned n = 0; n < 16; ++n)
			*to++ += *from++;
		return *this;
	}

	qnMtx4& qnMtx4::Sub(const qnMtx4& rhs)
	{
		float* to = data.a[0];
		const float* from = rhs.data.a[0];
		for (unsigned n = 0; n < 16; ++n)
			*to++ -= *from++;
		return *this;
	}

	qnMtx4& qnMtx4::Mul(const qnMtx4& rhs)
	{
		float f[4];
		for (unsigned row = 0; row < 4; ++row)
		{
			memcpy(f, data.a[row], sizeof(float) * 4);
			for (unsigned n = 0; n < 4; ++n)
				data.a[row][n] = f[0] * rhs.data.a[0][n] + f[1] * rhs.data.a[1][n] + f[2] * rhs.data.a[2][n] + f[3] * rhs.data.a[3][n];
		}
		return *this;
	}

	qnMtx4& qnMtx4::Transpose()
	{
		fswap(data.mtx._01, data.mtx._10);
		fswap(data.mtx._02, data.mtx._20);
		fswap(data.mtx._03, data.mtx._30);
		fswap(data.mtx._12, data.mtx._21);
		fswap(data.mtx._13, data.mtx._31);
		fswap(data.mtx._23, data.mtx._32);
		return *this;
	}

#pragma warning ( push )
#pragma warning ( disable: 4700 )
	qnMtx4& qnMtx4::Invert()
	{
		float* src = &data.a[0][0];
		__m128 minor0, minor1, minor2, minor3;
		__m128 row0, row1, row2, row3;
		__m128 det, tmp1;
#if !defined NDEBUG || defined STATIC
		// Suppress RTC error for uninit vars
		float init = 0.f;
		row3 = row1 = tmp1 = _mm_load_ps1(&init);
#endif // NDEBUG
		tmp1 = _mm_loadh_pi(_mm_loadl_pi(tmp1, (__m64*)(src)), (__m64*)(src + 4));
		row1 = _mm_loadh_pi(_mm_loadl_pi(row1, (__m64*)(src + 8)), (__m64*)(src + 12));
		row0 = _mm_shuffle_ps(tmp1, row1, 0x88);
		row1 = _mm_shuffle_ps(row1, tmp1, 0xDD);
		tmp1 = _mm_loadh_pi(_mm_loadl_pi(tmp1, (__m64*)(src + 2)), (__m64*)(src + 6));
		row3 = _mm_loadh_pi(_mm_loadl_pi(row3, (__m64*)(src + 10)), (__m64*)(src + 14));
		row2 = _mm_shuffle_ps(tmp1, row3, 0x88);
		row3 = _mm_shuffle_ps(row3, tmp1, 0xDD);
		// -----------------------------------------------
		tmp1 = _mm_mul_ps(row2, row3);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
		minor0 = _mm_mul_ps(row1, tmp1);
		minor1 = _mm_mul_ps(row0, tmp1);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
		minor0 = _mm_sub_ps(_mm_mul_ps(row1, tmp1), minor0);
		minor1 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor1);
		minor1 = _mm_shuffle_ps(minor1, minor1, 0x4E);
		// -----------------------------------------------
		tmp1 = _mm_mul_ps(row1, row2);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
		minor0 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor0);
		minor3 = _mm_mul_ps(row0, tmp1);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
		minor0 = _mm_sub_ps(minor0, _mm_mul_ps(row3, tmp1));
		minor3 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor3);
		minor3 = _mm_shuffle_ps(minor3, minor3, 0x4E);
		// -----------------------------------------------
		tmp1 = _mm_mul_ps(_mm_shuffle_ps(row1, row1, 0x4E), row3);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
		row2 = _mm_shuffle_ps(row2, row2, 0x4E);
		minor0 = _mm_add_ps(_mm_mul_ps(row2, tmp1), minor0);
		minor2 = _mm_mul_ps(row0, tmp1);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
		minor0 = _mm_sub_ps(minor0, _mm_mul_ps(row2, tmp1));
		minor2 = _mm_sub_ps(_mm_mul_ps(row0, tmp1), minor2);
		minor2 = _mm_shuffle_ps(minor2, minor2, 0x4E);
		// -----------------------------------------------
		tmp1 = _mm_mul_ps(row0, row1);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
		minor2 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor2);
		minor3 = _mm_sub_ps(_mm_mul_ps(row2, tmp1), minor3);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
		minor2 = _mm_sub_ps(_mm_mul_ps(row3, tmp1), minor2);
		minor3 = _mm_sub_ps(minor3, _mm_mul_ps(row2, tmp1));
		// -----------------------------------------------
		tmp1 = _mm_mul_ps(row0, row3);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
		minor1 = _mm_sub_ps(minor1, _mm_mul_ps(row2, tmp1));
		minor2 = _mm_add_ps(_mm_mul_ps(row1, tmp1), minor2);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
		minor1 = _mm_add_ps(_mm_mul_ps(row2, tmp1), minor1);
		minor2 = _mm_sub_ps(minor2, _mm_mul_ps(row1, tmp1));
		// -----------------------------------------------
		tmp1 = _mm_mul_ps(row0, row2);
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0xB1);
		minor1 = _mm_add_ps(_mm_mul_ps(row3, tmp1), minor1);
		minor3 = _mm_sub_ps(minor3, _mm_mul_ps(row1, tmp1));
		tmp1 = _mm_shuffle_ps(tmp1, tmp1, 0x4E);
		minor1 = _mm_sub_ps(minor1, _mm_mul_ps(row3, tmp1));
		minor3 = _mm_add_ps(_mm_mul_ps(row1, tmp1), minor3);
		// -----------------------------------------------
		det = _mm_mul_ps(row0, minor0);
		det = _mm_add_ps(_mm_shuffle_ps(det, det, 0x4E), det);
		det = _mm_add_ss(_mm_shuffle_ps(det, det, 0xB1), det);
		tmp1 = _mm_rcp_ss(det);
		det = _mm_sub_ss(_mm_add_ss(tmp1, tmp1), _mm_mul_ss(det, _mm_mul_ss(tmp1, tmp1)));
		det = _mm_shuffle_ps(det, det, 0x00);
		minor0 = _mm_mul_ps(det, minor0);
		_mm_storel_pi((__m64*)(src), minor0);
		_mm_storeh_pi((__m64*)(src + 2), minor0);
		minor1 = _mm_mul_ps(det, minor1);
		_mm_storel_pi((__m64*)(src + 4), minor1);
		_mm_storeh_pi((__m64*)(src + 6), minor1);
		minor2 = _mm_mul_ps(det, minor2);
		_mm_storel_pi((__m64*)(src + 8), minor2);
		_mm_storeh_pi((__m64*)(src + 10), minor2);
		minor3 = _mm_mul_ps(det, minor3);
		_mm_storel_pi((__m64*)(src + 12), minor3);
		_mm_storeh_pi((__m64*)(src + 14), minor3);

		return *this;
	}
#pragma warning ( pop )

	qnMtx4& qnMtx4::Stabelize()
	{
		for (unsigned n = 0; n < 16; ++n)
		{
			if (fabs(data.f[n]) < GML_EPSILON_F)
				data.f[n] = 0;
		}
		return *this;
	}

	// non Member
	qnMtx4 qnMtx4::Scale(const qnMtx4& mtx, const float f)
	{
		qnMtx4 ret(mtx);
		return ret.Scale(f);
	}

	qnMtx4 qnMtx4::Translate(const qnMtx4& mtx, const qnVec3& vec)
	{
		qnMtx4 ret(mtx);
		return ret.Translate(vec);
	}

	qnMtx4 qnMtx4::Add(const qnMtx4& lhs, const qnMtx4& rhs)
	{
		qnMtx4 ret(lhs);
		return ret.Add(rhs);
	}

	qnMtx4 qnMtx4::Sub(const qnMtx4& lhs, const qnMtx4& rhs)
	{
		qnMtx4 ret(lhs);
		return ret.Sub(rhs);
	}

	qnMtx4 qnMtx4::Mul(const qnMtx4& lhs, const qnMtx4& rhs)
	{
		qnMtx4 ret(lhs);
		return ret.Mul(rhs);
	}

	qnMtx4 qnMtx4::Transpose(const qnMtx4& mtx)
	{
		qnMtx4 ret(mtx);
		return ret.Transpose();
	}

	qnMtx4 qnMtx4::Invert(const qnMtx4& mtx)
	{
		qnMtx4 ret(mtx);
		return ret.Invert();
	}


	qnMtx4 qnMtx4::RotateX(float angle)
	{
		return qnMtx4(1, 0, 0, 0,
			0, cos(angle), sin(angle), 0,
			0, -sin(angle), cos(angle), 0,
			0, 0, 0, 1);
	}

	qnMtx4 qnMtx4::RotateY(float angle)
	{
		return qnMtx4(cos(angle), 0, -sin(angle), 0,
			0, 1, 0, 0,
			sin(angle), 0, cos(angle), 0,
			0, 0, 0, 1);
	}

	qnMtx4 qnMtx4::RotateZ(float angle)
	{
		return qnMtx4(cos(angle), sin(angle), 0, 0,
			-sin(angle), cos(angle), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
	}

	qnMtx4 qnMtx4::RotateAxisAngle(qnVec3 v, const float angle)
	{
		float sina = sin(angle);
		float cosa = cos(angle);
		float cosa_1 = 1 - cosa;
		v.Normalize();
		float x2 = v.x * v.x * cosa_1, y2 = v.y * v.y * cosa_1, z2 = v.z * v.z * cosa_1;
		float xy = v.x * v.y * cosa_1, xz = v.x * v.z * cosa_1, yz = v.y * v.z * cosa_1;
		float xsin = v.x * sina, ysin = v.y * sina, zsin = v.z * sina;
		return qnMtx4(cosa + x2, xy + zsin, xz - ysin, 0,
			xy - zsin, cosa + y2, yz + xsin, 0,
			xz + ysin, yz - xsin, cosa + z2, 0,
			0, 0, 0, 1);
	}

	qnMtx4 qnMtx4::RotateAxisAngle(qnVec4 axis, const float angle)
	{
		return RotateAxisAngle(qnVec3(axis.x, axis.y, axis.z), angle);
	}

	void qnMtx4::GetAxisAngle(const qnMtx4& mtx, qnVec3* pAxis, float* pAngle)
	{
		if (pAngle)
			*pAngle = acos((mtx.data.mtx._00 + mtx.data.mtx._11 + mtx.data.mtx._22 - 1) / 2);
		if (pAxis)
		{
			pAxis->x = mtx.data.mtx._12 - mtx.data.mtx._21;
			pAxis->y = mtx.data.mtx._20 - mtx.data.mtx._02;
			pAxis->z = mtx.data.mtx._01 - mtx.data.mtx._10;
			pAxis->Normalize();
		}
	}

	void qnMtx4::GetAxisAngle(const qnMtx4& mtx, qnVec4* pAxis, float* pAngle)
	{
		qnVec3 v;
		float a;
		GetAxisAngle(mtx, &v, &a);
		if (pAxis)
			*pAxis = qnVec4(v.x, v.y, v.z, 0);
		if (pAngle)
			*pAngle = a;
	}

	qnMtx4 qnMtx4::RotateEuler(float yaw, float pitch, float roll)
	{
		float cy = cos(yaw), sy = sin(yaw);
		float cp = cos(pitch), sp = sin(pitch);
		float cr = cos(roll), sr = sin(roll);

		return qnMtx4(cr * cy + sr * sp * sy, sr * cp, sr * sp * cy - cr * sy, 0,
			cr * sp * sy - sr * cy, cr * cp, sr * sy + cr * sp * cy, 0,
			cp * sy, -sp, cp * cy, 0,
			0, 0, 0, 1);
	}

	qnMtx4 qnMtx4::RotateQuaternion(const qnQuat& quat)
	{
		return quat.GetMatrix();
	}

	qnMtx4 qnMtx4::Stabelize(const qnMtx4& mtx)
	{
		return qnMtx4(mtx).Stabelize();
	}

	bool GML_EXPORT operator== (const qnMtx4& lhs, const qnMtx4& rhs)
	{
		for (unsigned n = 0; n < 16; ++n)
		{
			if (!fcmp(lhs.data.f[n], rhs.data.f[n]))
				return false;
		}
		return true;
	}
}
