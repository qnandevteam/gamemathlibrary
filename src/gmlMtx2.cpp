#include "gmlMtx2.h"

#include <cmath>

#include "gmlMath.h"

namespace gml
{
	const qnMtx2 qnMtx2::ZERO = qnMtx2(0, 0, 0, 0);
	const qnMtx2 qnMtx2::ONE = qnMtx2(1, 1, 1, 1);
	const qnMtx2 qnMtx2::FLIP_X = qnMtx2(-1, 0, 0, 1);
	const qnMtx2 qnMtx2::FLIP_Y = qnMtx2(1, 0, 0, -1);
	const qnMtx2 qnMtx2::IDENTITY = qnMtx2(1, 0, 0, 1);

	qnMtx2::qnMtx2(float a, float b, float c, float d)
	{
		data.mtx._00 = a;
		data.mtx._01 = b;
		data.mtx._10 = c;
		data.mtx._11 = d;
	}

	qnMtx2::qnMtx2(const qnMtx2& rhs)
	{
		*this = rhs;
	}

	qnMtx2& qnMtx2::operator= (const qnMtx2& rhs)
	{
		data.mtx._00 = rhs.data.mtx._00;
		data.mtx._01 = rhs.data.mtx._01;
		data.mtx._10 = rhs.data.mtx._10;
		data.mtx._11 = rhs.data.mtx._11;
		return *this;
	}

	// Members work in place
	qnMtx2& qnMtx2::Scale(const float f)
	{
		for (unsigned n = 0; n < 4; ++n)
			data.f[n] *= f;
		return *this;
	}

	qnMtx2& qnMtx2::Add(const qnMtx2& rhs)
	{
		for (unsigned n = 0; n < 4; ++n)
			data.f[n] += rhs.data.f[n];
		return *this;
	}

	qnMtx2& qnMtx2::Sub(const qnMtx2& rhs)
	{
		for (unsigned n = 0; n < 4; ++n)
			data.f[n] -= rhs.data.f[n];
		return *this;
	}

	qnMtx2& qnMtx2::Mul(const qnMtx2& rhs)
	{
		float tmp[4] = {};
		tmp[0] = (data.mtx._00 * rhs.data.mtx._00) + (data.mtx._01 * rhs.data.mtx._10);
		tmp[1] = (data.mtx._00 * rhs.data.mtx._01) + (data.mtx._01 * rhs.data.mtx._11);
		tmp[2] = (data.mtx._10 * rhs.data.mtx._00) + (data.mtx._11 * rhs.data.mtx._10);
		tmp[3] = (data.mtx._10 * rhs.data.mtx._01) + (data.mtx._11 * rhs.data.mtx._11);
		for (unsigned n = 0; n < 4; ++n)
			data.f[n] = tmp[n];
		return *this;
	}

	qnMtx2& qnMtx2::Transpose()
	{
		float tmp = data.mtx._01;
		data.mtx._01 = data.mtx._10;
		data.mtx._10 = tmp;
		return *this;
	}

	qnMtx2& qnMtx2::Invert()
	{
		return (*this = qnMtx2::Invert(*this));
	}

	qnMtx2& qnMtx2::Stabelize()
	{
		for (unsigned n = 0; n < 4; ++n)
		{
			if (fabs(data.f[n]) < GML_EPSILON_F)
				data.f[n] = 0.f;
		}
		return *this;
	}

	// non Member
	qnMtx2 qnMtx2::Scale(const qnMtx2& mtx, const float f)
	{
		return qnMtx2(mtx).Scale(f);
	}

	qnMtx2 qnMtx2::Add(const qnMtx2& lhs, const qnMtx2& rhs)
	{
		return qnMtx2(lhs).Add(rhs);
	}

	qnMtx2 qnMtx2::Sub(const qnMtx2& lhs, const qnMtx2& rhs)
	{
		return qnMtx2(lhs).Sub(rhs);
	}

	qnMtx2 qnMtx2::Mul(const qnMtx2& lhs, const qnMtx2& rhs)
	{
		return qnMtx2(lhs).Mul(rhs);
	}

	qnMtx2 qnMtx2::Transpose(const qnMtx2& mtx)
	{
		return qnMtx2(mtx).Transpose();
	}

	qnMtx2 qnMtx2::Invert(const qnMtx2& mtx)
	{
		float det = (mtx.data.mtx._00 * mtx.data.mtx._11) - (mtx.data.mtx._01 * mtx.data.mtx._10);
		if (det)
		{
			qnMtx2 ret = qnMtx2(mtx.data.mtx._11, -mtx.data.mtx._01, -mtx.data.mtx._10, mtx.data.mtx._00);
			for (unsigned n = 0; n < 4; ++n)
				ret.data.f[n] /= det;
			return ret;
		}
		return qnMtx2::ZERO;
	}

	qnMtx2 qnMtx2::Rotation(float angle)
	{
		return qnMtx2(
			cos(angle), sin(angle),
			-sin(angle), cos(angle)
		);
	}

	qnMtx2 qnMtx2::Stabelize(const qnMtx2& mtx)
	{
		return qnMtx2(mtx).Stabelize();
	}

	bool GML_EXPORT operator== (const qnMtx2& lhs, const qnMtx2& rhs)
	{
		for (unsigned n = 0; n < 4; ++n)
		{
			if (!fcmp(lhs.data.f[n], rhs.data.f[n]))
				return false;
		}
		return true;
	}
} // namespace gml
